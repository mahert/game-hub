import {
  FaWindows,
  FaAndroid,
  FaLinux,
  FaXbox,
  FaApple,
  FaPlaystation,
} from "react-icons/fa";

import { MdPhoneIphone } from "react-icons/md";

import { SiNintendo } from "react-icons/si";

import { BsGlobe } from "react-icons/bs";

import { Platform } from "../hooks/useGames";
import { HStack, Icon } from "@chakra-ui/react";
import { IconType } from "react-icons";

interface Props {
  platforms: Platform[];
}

const PlatformIconList = ({ platforms }: Props) => {
  const iconMap: { [key: string]: IconType } = {
    pc: FaWindows,
    playstation: FaPlaystation,
    xbox: FaXbox,
    nintendo: SiNintendo,
    mac: FaApple,
    linux: FaLinux,
    ios: MdPhoneIphone,
    web: BsGlobe,
    android: FaAndroid,
  };

  return (
    <HStack marginY={1}>
      {platforms.map((it) => (
        <Icon color="gray.500" as={iconMap[it.slug]} />
      ))}
    </HStack>
  );
};

export default PlatformIconList;
