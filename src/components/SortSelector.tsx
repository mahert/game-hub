import { Menu, MenuButton, Button, MenuList, MenuItem } from "@chakra-ui/react";
import { BsChevronDown } from "react-icons/bs";

interface Props {
  setSortBy: (it: string) => void;
  sortBy?: string;
}

const sortSelectors = [
  { value: "", key: "Relevence" },
  { value: "-addded", key: "Date Added" },
  { value: "name", key: "Name" },
  { value: "-release", key: "Release Date" },
  { value: "-metacritic", key: "Popularity" },
  { value: "-rating", key: "Average Rating" },
];

const SortSelector = ({ setSortBy, sortBy }: Props) => {
  const currentSortOrder = sortSelectors.find((it) => it.value == sortBy);

  return (
    <Menu>
      <MenuButton as={Button} rightIcon={<BsChevronDown />}>
        Order by: {currentSortOrder?.key || "Relevance"}
      </MenuButton>
      <MenuList>
        {sortSelectors.map((it) => (
          <MenuItem key={it.value} onClick={() => setSortBy(it.value)}>
            {it.key}
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export default SortSelector;
