import { Badge } from "@chakra-ui/react";

interface Prop {
  score: number;
}

const CriticScore = ({ score }: Prop) => {
  const color = score > 75 ? "green" : "yellow";

  return (
    <Badge fontSize="14px" paddingX={2} borderRadius={1} colorScheme={color}>
      {score}{" "}
    </Badge>
  );
};

export default CriticScore;
