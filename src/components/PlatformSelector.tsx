import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { BsChevronDown } from "react-icons/bs";
import usePlatforms from "../hooks/usePlatforms";
import { Platform } from "../hooks/useGames";

interface Porps {
  onSelect: (it: Platform) => void;
  platform?: Platform;
}

const PlatformSelector = ({ onSelect, platform }: Porps) => {
  const { data, error } = usePlatforms();

  if (error) return null;

  return (
    <Menu>
      <MenuButton as={Button} rightIcon={<BsChevronDown />}>
        {platform ? platform.name : "Platform"}
      </MenuButton>
      <MenuList>
        {data.map((it) => (
          <MenuItem key={it.id} onClick={() => onSelect(it)}>
            {it.name}
          </MenuItem>
        ))}
      </MenuList>
    </Menu>
  );
};

export default PlatformSelector;
