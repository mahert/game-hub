import { GameQuery } from '../App';
import useData from './useData';


export interface Platform {
    id: number,
    name: string,
    slug: string
}

export interface Game {
    id: number;
    name: string;
    background_image: string
    parent_platforms: { platform: Platform }[]
    metacritic: number
    rating_top: number
}


const useGames = (gameQuery: GameQuery, platform?: Platform) => useData<Game>("games", { params: { genres: gameQuery.genre?.id, platforms: platform?.id, ordering: gameQuery.sortBy, search: gameQuery.search } }, [gameQuery])

export default useGames
